package com.example.calculadora_caio_vinicius

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    lateinit var txtDI: TextView
    lateinit var txtDH: TextView
    var lastDot: Boolean = false
    //    modo resultado = true - modo calculo = false
    var state = true
    var n1 = 0.0
    var n2 = 0.0
    var op = ""
    var aux = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        state = true
        txtDI = findViewById(R.id.displayInput)
        txtDH = findViewById(R.id.displayHistory)

    }

    fun numberAction (v: View){
        if (state) {
            txtDI.text = (v as Button).text
            state = false
        }
        else {
            txtDI.append((v as Button).text)
        }
    }
    fun operationAction (v: View){
        if (!state) {
            if (op == ""){
                n1 = txtDI.text.toString().toDouble()
                op = (v as Button).text.toString()
                txtDH.append(" $n1 $op")
            }
            else {
                n2 = txtDI.text.toString().toDouble()
                n1 = when (op) {
                    "+" -> n1 + n2
                    "÷" -> n1 / n2
                    "×" -> n1 * n2
                    "-" -> n1 - n2
                    else -> n1
                }
                n2 = 0.0
                op = (v as Button).text.toString()
                txtDI.text = "$n1"
                txtDH.text = "$n1 $op"
            }
        }
        else {
            if (op != "") {
                n1 = txtDI.text.toString().toDouble()
                op = (v as Button).text.toString()
                txtDH.text = (" $n1 $op")
            }
        }
        state = true
    }
    fun equalsAction (v: View){
        if (!state){
            if (op != "") {
                n2 = txtDI.text.toString().toDouble()
                txtDH.text = "$n1 $op "
                n1 = when (op) {
                    "+" -> n1 + n2
                    "÷" -> n1 / n2
                    "×" -> n1 * n2
                    "-" -> n1 - n2
                    else -> n1
                }
                txtDI.text = "$n1"
                txtDH.append("$n2 = $n1")
            }
        }
        if (state && n2 != 0.0){
            txtDH.text = "$n1 $op "
            n1 = when (op) {
                "+" -> n1 + n2
                "÷" -> n1 / n2
                "×" -> n1 * n2
                "-" -> n1 - n2
                else -> n1
            }
            txtDI.text = "$n1"
            txtDH.append("$n2 = $n1")
        }
        if (state && op != "" && n2 == 0.0) {
            n2 = txtDI.text.toString().toDouble()
            if(!aux){
                n1 = n2
            }

            txtDH.text = "$n1 $op "
            n1 = when (op) {
                "+" -> n1 + n2
                "÷" -> n1 / n2
                "×" -> n1 * n2
                "-" -> n1 - n2
                else -> n1
            }
            txtDI.text = "$n1"
            txtDH.append("$n2 = $n1")
        }
        aux = false
        state = true
    }
    fun dotAction (v: View){
       var temp = txtDI.text
       var i = 0

       while(i < temp.length){
           if (temp.get(i).toString() == "."){
                temp = "."
           }
           i++
       }

       if(temp != "."){
           if (state) {
               txtDI.text = "0"
               state = false
           }
           else {
               txtDI.append((v as Button).text)
           }

       }
   }
    fun clearAction (v: View){
        txtDH.text = ""
        txtDI.text = "0"
        state = true
        n1 = 0.0
        n2 = 0.0
        op = ""
    }
    fun eraseAction (v: View){
        var temp = txtDI.text

        if(temp.length == 1) {
            txtDI.text = temp.replaceFirst(".$".toRegex(), "0")
            state = true
            n2 = 0.0
            aux = true
        }
        else{
            txtDI.text = temp.replaceFirst(".$".toRegex(), "")
        }

    }
}
